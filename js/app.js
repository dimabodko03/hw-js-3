const num = +prompt('Write your number');

if(num < 5) {
    console.log('Sorry, no numbers');
} else {
    for(let i = 1; i < num; i++) {
        if (i % 5 === 0) {
            console.log('Numbers divisible by 5:' + i);
        } 
    }
}
